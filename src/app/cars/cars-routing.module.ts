import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {CarDetailsComponent} from './cars-components/car-details/car-details.component';
import {CarResolve} from './cars-services/car-resolve.service';


const CARS_ROUTES: Route[] = [
  {
    path: 'cars/:id',
    component: CarDetailsComponent,
    resolve: {car: CarResolve}
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(CARS_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})

export class CarsRoutingModule {
}
