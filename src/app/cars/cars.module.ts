import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarsListComponent } from './cars-components/cars-list/cars-list.component';
import { TotalCostComponent } from './cars-components/total-cost/total-cost.component';
import {SharedModule} from '../shared-module/shared.module';
import { CarDetailsComponent } from './cars-components/car-details/car-details.component';
import {RouterModule} from '@angular/router';
import {CarResolve} from './cars-services/car-resolve.service';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [CarsListComponent],
  providers: [CarResolve],
  declarations: [CarsListComponent, TotalCostComponent, CarDetailsComponent]
})
export class CarsModule { }
